namespace = flavor_toarnen

# The Whitecliffs burghers uprising of 1356   
country_event = {
	id = flavor_toarnen.1
	title = flavor_toarnen.1.t
	desc = flavor_toarnen.1.d
	picture = DIPLOMACY_eventPicture
    
	is_triggered_only = yes

	trigger = {
		tag = A57
	}


	option = {
		name = flavor_toarnen.1.a
		921 = {  # bordercliff
			add_province_modifier = {
				name = A57_charters_of_bordercliff
				duration = -1
			}
		}
	}	
}

#A very Roilsardi party 
country_event = {
	id = flavor_toarnen.2
	title = flavor_toarnen.2.t
	desc = flavor_toarnen.2.d
	picture = DIPLOMACY_eventPicture
    
	fire_only_once = yes
	is_triggered_only = yes  
	
    option = {
		name = flavor_toarnen.2.a		
		
		add_estate_loyalty_modifier = {
			estate = estate_nobles
			desc = "A very Roilsardi party"
			duration = 1825
			loyalty = 15
		}
	}
}


#A visit to Saloren 
country_event = {

	id = flavor_toarnen.3
	title = flavor_toarnen.3.t
	desc = flavor_toarnen.3.d
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = flavor_toarnen.3.a

		define_consort = {
			name = "Laurens"
			dynasty = "Sil na Loop"
			age = 16
			female = yes
			adm = 4
			dip = 1
			mil = 4
			country_of_origin = A87
			culture = roilsardi
		}

		define_heir = {
			dynasty = "Sil na Loop"
			age = 0
			male = yes
			adm = 1
			dip = 1
			mil = 1
			culture = roilsardi
		}
	}
    option = {
		name = flavor_toarnen.3.b

		define_consort = {
			name = "Maurise"
			dynasty = "Sil na Loop"
			age = 20
			male = yes
			adm = 3
			dip = 4
			mil = 2
			country_of_origin = A87
			culture = roilsardi
		}

		define_heir = {
			dynasty = "Sil na Loop"
			age = 0
			male = yes
			adm = 1
			dip = 1
			mil = 1
			culture = roilsardi
		}
	}
    option = {
		name = flavor_toarnen.3.c

		add_prestige = 20
	}
}



#The Royal Chess Contest, start event

country_event = {
	id = flavor_toarnen.4
	title = flavor_toarnen.4.t
	desc = flavor_toarnen.4.d
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes
	is_triggered_only = yes

    option = {
		name = flavor_toarnen.4.a

		hidden_effect = {
			if = {
				limit = {
					dip = 4
				}
				country_event = {
					id = flavor_toarnen.6
					days = 30
				}
			}
			else = {
				country_event = {
					id = flavor_toarnen.5
					days = 30
				}
			}
		}
	}
}

#The Royal Chess Contest, if ruler DIP under 4  

country_event = {
	id = flavor_toarnen.5
	title = flavor_toarnen.5.t
	desc = flavor_toarnen.5.d
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes
	is_triggered_only = yes

    option = {
		name = flavor_toarnen.5.a

		increase_ruler_dip_effect = yes
	}
}


#The Royal Chess Contest, if ruler DIP over 4  

country_event = {
	id = flavor_toarnen.6
	title = flavor_toarnen.6.t
	desc = flavor_toarnen.6.d
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes
	is_triggered_only = yes

    option = {
		name = flavor_toarnen.6.a

		add_prestige = 20
		add_legitimacy = 10
		add_country_modifier = {
			name = A57_winner_of_aiscestir_chess_contest
			duration = 1825
		}
	}
}


#Roilsardi Migration event

country_event = {
	id = flavor_toarnen.7
	title = flavor_toarnen.7.t
	desc = flavor_toarnen.7.d
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = flavor_toarnen.7.a
		add_treasury = -100
		add_dip_power = -100
		add_country_modifier = {
			name = A57_ongoing_migration
			duration = 1825
		}
		hidden_effect = {
			set_country_flag = A57_small_migration
			country_event = {
				id = flavor_toarnen.11
				days = 1825
			}
		}
	}

	option = {
		name = flavor_toarnen.7.b
		add_treasury = -200
		add_dip_power = -200
		add_country_modifier = {
			name = A57_ongoing_migration
			duration = 3650
		}
		hidden_effect = {
			country_event = {
				id = flavor_toarnen.11
				days = 3650
			}
		}
	}
}

#The End of the Vampires

country_event = {
	id = flavor_toarnen.8
	title = flavor_toarnen.8.t
	desc = flavor_toarnen.8.d
	picture = DIPLOMACY_eventPicture

	fire_only_once = yes

	trigger = {
		tag = A57
		mission_completed = A57_the_crusade
		441 = {  # Arca Corvur
			controlled_by = A57
			owned_by = A59  # Corvuria
		}
	}

	option = {
		name = flavor_toarnen.8.a

		add_manpower = -1
		441 = {
			add_base_tax = -3
			add_base_manpower = -3
			add_base_production = -3
		}
		A59 = {
			kill_heir = yes
			kill_vampire_ruler = yes
		}
		add_treasury = 450

		if = {
			limit = {
				army_strength = {
					who = A59  # Corvuria
					value = 4
				}
			}

			country_event = {
				id = flavor_toarnen.9
			}
		}
	}
}


#Corvuria Shattered 

country_event = {
	id = flavor_toarnen.9
	title = flavor_toarnen.9.t
	desc = flavor_toarnen.9.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_toarnen.9.a
		
		create_subject = {
			subject_type = vassal
			subject = A59  # Corvuria
		}
		A59 = {
			random_owned_province = {
				spawn_rebels = {
					type = noble_rebels
					size = 2
				}
			}
		}
		white_peace = A59
	}

	option = {
		name = flavor_toarnen.9.b
		
		create_subject = {
			subject_type = march
			subject = A59  # Corvuria
		}
		A59 = {
			random_owned_province = {
				spawn_rebels = {
					type = anti_tax_rebels
					size = 1
				}
			}
			add_liberty_desire = -15
		}
		white_peace = A59
	}

	option = {
		name = flavor_toarnen.9.c
		
		add_prestige = -10
	}
}

#Margraves of Toarnaiste
country_event = {
	id = flavor_toarnen.10
	title = flavor_toarnen.10.t
	desc = flavor_toarnen.10.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_toarnen.10.a

		add_prestige = 40
		add_country_modifier = {
			name = A57_toarnaiste_the_eastern_thorn
			duration = -1
		}

		custom_tooltip = toaren_roilsard_culture_tooltip
		hidden_effect = {
			capital_scope = {
				add_province_triggered_modifier = toaren_roilsard_culture1
				add_province_triggered_modifier = toaren_roilsard_culture2
				add_province_triggered_modifier = toaren_roilsard_culture3
				add_province_triggered_modifier = toaren_roilsard_culture4
				add_province_triggered_modifier = toaren_roilsard_culture5
			}
		}
	}
}


#Organizing the Settlements
country_event = {
	id = flavor_toarnen.11
	title = flavor_toarnen.11.t
	desc = flavor_toarnen.11.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_toarnen.11.a

		if = {
			limit = {
				has_country_flag = A57_small_migration
			}

			420 = {  # Aenawick
				change_culture = roilsardi
				add_base_tax = 2
				add_base_production = 1
				add_base_manpower = 1
			}
		}
		else = {
			420 = {  # Aenawick
				change_culture = roilsardi
				add_base_tax = 2
				add_base_production = 2
				add_base_manpower = 1
			}
			922 = {  # Aiscestir
				add_base_tax = 2
				add_base_production = 1
			}
			add_country_modifier = {
				name = A57_unprepared_administration
				duration = 3650
			}
		}
	}
}



#We are the true roilsardis
country_event = {
	id = flavor_toarnen.12
	title = flavor_toarnen.12.t
	desc = flavor_toarnen.12.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_toarnen.12.a

		every_owned_province = {
			limit = {
				culture = roilsardi
			}
			owner = {
				add_legitimacy = 2
				add_prestige = 3
			}
		}
	}
}

#Celliande threaten war Celliande
country_event = {
	id = flavor_toarnen.13
	title = flavor_toarnen.13.t
	desc = flavor_toarnen.13.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_toarnen.13.a

		ai_chance = {
			factor = 30
			modifier = {
				factor = 100
				is_at_war = yes
			}
		}

		A57 = {
			country_event = {
				id = flavor_toarnen.15
				days = 10
			}
		}
	}

	option = {
		name = flavor_toarnen.13.b

		ai_chance = {
			factor = 70
		}

		A57 = {
			country_event = {
				id = flavor_toarnen.14
				days = 10
			}
		}
	}
}

#Celliande threaten war Toarren
country_event = {
	id = flavor_toarnen.14
	title = flavor_toarnen.14.t
	desc = flavor_toarnen.14.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_toarnen.14.a

		ai_chance = {
			factor = 50
		}

		add_prestige = 20
		declare_war_with_cb = {
			casus_belli = cb_conquest
			who = A56
			war_goal_province = 413
		}
	}

	option = {
		name = flavor_toarnen.14.b

		ai_chance = {
			factor = 50
		}

		add_prestige = -10
	}
}

#Celliande threaten war Celliande gave up
country_event = {
	id = flavor_toarnen.15
	title = flavor_toarnen.15.t
	desc = flavor_toarnen.15.d
	picture = DIPLOMACY_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_toarnen.15.a

		413 = {  # Sorncell
			cede_province = A57
		}
		add_truce_with = A56
		A56 = {
			add_truce_with = A57
		}
	}
}